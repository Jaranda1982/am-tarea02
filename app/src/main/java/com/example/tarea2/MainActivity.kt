package com.example.tarea2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iniciarViews()
        iniciarContenedorTarea()
    }

    private fun iniciarViews(){
        //Asignando Evento al Boton Agregar
        fabAgregar.setOnClickListener{
            val tareaActivity = Intent(this, TareaActivity::class.java)
            startActivityForResult(tareaActivity, 1)
            //startActivity(personaActivity)
            //finish() Cierra la ventana padre que abrio esta ventana hijo
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode){
            1 ->{//Agregar Persona
                if (resultCode == Activity.RESULT_OK){// se agregó correctamente
                    val titulo = data?.getStringExtra("tareaTitulo") ?: "-"
                    val cuando = data?.getStringExtra("tareaCuando") ?: "-"
                    val lugar = data?.getStringExtra("tareaLugar") ?: "-"
                    val tarea = Tarea(titulo, cuando, lugar)
                    tareaAdapter.agregarTarea(tarea)
                }
            }
        }
    }
    private val tareaAdapter by lazy {
        TareaAdapter()
    }

    private fun iniciarContenedorTarea(){
        // Layout Manager
        rvTarea.layoutManager = LinearLayoutManager(this)

        // Adapter
        rvTarea.adapter = tareaAdapter //PersonasAdapter(personas.toMutableList())
    }
}
