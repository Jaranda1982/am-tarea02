package com.example.tarea2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_tarea.view.*

class TareaAdapter (
): RecyclerView.Adapter<TareaAdapter.TareasHolder>() {

    //val tareas: MutableList<String>? = null
    val tareas : MutableList<Tarea> = arrayListOf()

    fun agregarTarea(tarea:Tarea) {
        tareas.add(tarea)
        notifyItemInserted(itemCount - 1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TareasHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.cell_tarea, parent, false)
        return TareasHolder(view)
    }

    override fun getItemCount(): Int = tareas.size

    override fun onBindViewHolder(holder: TareasHolder, position: Int) {
        val tarea = tareas[position]
        holder.pintarTitulo(tarea.Titulo)
        holder.pintarCuando(tarea.Cuando)
        holder.pintarLugar(tarea.Lugar)
    }
    class TareasHolder(
        val miView: View
    ) : RecyclerView.ViewHolder(miView) {

        fun pintarTitulo(nombre: String) {
            miView.tvTitulo.text = nombre
        }
        fun pintarCuando(nombre: String) {
            miView.tvCuando.text = nombre
        }
        fun pintarLugar(nombre: String) {
            miView.tvLugar.text = nombre
        }
    }
}