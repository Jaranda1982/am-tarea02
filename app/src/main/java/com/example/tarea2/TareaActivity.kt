package com.example.tarea2

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_tarea.*


class TareaActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tarea)

        iniciarViews()
    }
    private fun iniciarViews(){
        btGuardar.setOnClickListener{
            val titulo = etTitulo.text.toString()
            val Lugar = etLugar.text.toString()
            //
            if (titulo.isNullOrBlank())
            {
                Toast.makeText(applicationContext,"Ingrese el Titulo.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (Lugar.isNullOrBlank())
            {
                Toast.makeText(applicationContext,"Ingrese el Lugar.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            //
            var rgcuandoValor: String = "-"
            if (rbHoy.isChecked())
            {
                rgcuandoValor = "Hoy"
            }
            else
            {
                rgcuandoValor = "Mañana"
            }
            //println("rgcuando")
            //println(rgcuando)
            val datos = Intent().apply {
                putExtra("tareaTitulo", titulo)
            }.apply {  putExtra("tareaCuando", rgcuandoValor)
            }.apply {  putExtra("tareaLugar", Lugar) }
            //
            setResult(Activity.RESULT_OK, datos) //Almacenamos Valores
            finish() //Cierra Venta
        }

    }
}
